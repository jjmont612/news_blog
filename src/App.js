import React, { Component, Fragment } from 'react';
import Header from './components/Header';
import ListaNoticias from './components/ListaNoticias';
import Formulario from './components/Formulario';

class App extends Component {
  state = {
    noticias: []
  }

  async componentDidMount() {
    this.consultarNoticias();
  }

  consultarNoticias = async (categoria='general') => {
    const url = `https://newsapi.org/v2/top-headlines?country=us&category=${categoria}&apiKey=e74a4856e16049198e24b73f54bfdc2c`;

    const res = await fetch(url);
    const noticias = await res.json();
    // console.log(news.articles);
    
    this.setState({
      noticias : noticias.articles
    })
  }

  render(){
    return (
      <Fragment>
        <Header
          titulo = 'USA News'
        />
        <div className="container white contenedor-noticias">
            <Formulario
              consultarNoticias={this.consultarNoticias}
            />
          <ListaNoticias
            noticias = {this.state.noticias}
          />
        </div>
      </Fragment>
    );
  }
}

export default App;


// api key = e74a4856e16049198e24b73f54bfdc2c
